package sda.ex;

import java.util.*;

public class VendingMachineContainer implements IContainer {

    private Map<String, Dispenser> stock;

    VendingMachineContainer() {
        this.stock = createStock();
    }

    private Map<String, Dispenser> createStock() {
        List<Product> products = new ArrayList<>();
        products.add(new Product("Coca cola", 199, ProductType.DRINK));
        products.add(new Product("Coca cola light", 219, ProductType.DRINK));
        products.add(new Product("Sprite", 149, ProductType.SWEET));
        products.add(new Product("Mars", 199, ProductType.SWEET));

        Map<String, Dispenser> stockMap = new LinkedHashMap<>();
        stockMap.put("4F", new Dispenser(products.get(0),10));
        stockMap.put("2B", new Dispenser(products.get(1),5));
        stockMap.put("1C", new Dispenser(products.get(1),8));
        stockMap.put("3A", new Dispenser(products.get(2),3));
        stockMap.put("2A", new Dispenser(products.get(2),7));
        stockMap.put("1D", new Dispenser(products.get(3),5));
        stockMap.put("4A", new Dispenser(products.get(3),4));

        return stockMap;
    }

    @Override
    public int getPrice(String dispenserId) {
        return stock
                .get(dispenserId)
                .getProduct()
                .getPrice();
    }

    @Override
    public Map<String, Dispenser> getStock() {
        return stock;
    }
}
