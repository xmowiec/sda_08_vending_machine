package sda.ex;

import java.util.*;

public class VendingMachineWallet implements IWallet {
    private Map<Coins, Integer> vault;
    private Map<Coins, Integer> rest;
    private Integer[] vaultSort;

    VendingMachineWallet() {
        this.vault = createBaseVault();
        this.rest = new HashMap<>();
    }

    private Map<Coins, Integer> createBaseVault() {
        Map<Coins, Integer> tempVault = new HashMap<>();
        tempVault.put(Coins.GR_1, 0);
        tempVault.put(Coins.GR_20, 10);
        tempVault.put(Coins.GR_50, 1);
        tempVault.put(Coins.ZL_1, 2);
        return tempVault;
    }

    public Map<Coins, Integer> getVault() {
        return vault;
    }

    @Override
    public Map<Coins, Integer> getCoinsMapToPayAmount(int amount) {
        vaultSort = convertMapIntoSortArray(vault);
        return getCoinsMapToPayAmountFromSortArray(amount, 0);
    }

    private Map<Coins, Integer> getCoinsMapToPayAmountFromSortArray(int amount, int n) {

        Map<Coins, Integer> result = new HashMap<>();

        // get the biggest available coin,
        // which value is less than amount
        while (vaultSort[n] > amount) {
            n++;
            if (n == vaultSort.length) {
                // there's no more coins
                return result;
            }
        }

        Integer coinValue = vaultSort[n];
        if (coinValue == amount) {
            // this coin exactly fulfill the amount
            result.put(Coins.getCoin(coinValue), 1);
        } else {
            // recursively call getResult
            do {
                coinValue = vaultSort[n];
                result = getCoinsMapToPayAmountFromSortArray(amount - coinValue, ++n);
                if (n == vaultSort.length) {
                    // there's no more coins
                    return result;
                }
            } while (result == null);

            Coins coin = Coins.getCoin(coinValue);
            if (!result.containsKey(coin)) {
                result.put(coin, 1);
            } else {
                Integer coinAmount = result.get(coin);
                result.put(coin, ++coinAmount);
            }
        }
        return result;
    }

    private Integer[] convertMapIntoSortArray(Map<Coins, Integer> vault) {
        List<Integer> vaultList = new ArrayList<>();
        for (Coins coin : vault.keySet()) {
            for (int i = 0; i < vault.get(coin); i++) {
                vaultList.add(coin.getValue());
            }
        }
        Integer[] vaultArray = new Integer[vaultList.size()];
        vaultList.toArray(vaultArray);
        Arrays.sort(vaultArray, Collections.reverseOrder());
        return vaultArray;
    }
}
