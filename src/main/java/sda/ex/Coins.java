package sda.ex;

import java.util.HashMap;
import java.util.Map;

public enum Coins {
    NOT_A_COIN(0),
    GR_1(1), GR_2(2), GR_5(5),
    GR_10(10), GR_20(20), GR_50(50),
    ZL_1(100), ZL_2(200), ZL_5(500);

    private int value;
    private static Map<Integer, Coins> coinsMap = new HashMap<>();

    // Assign coins to theirs values in map
    static {
        for (Coins coin : Coins.values()) {
            coinsMap.put(coin.value, coin);
        }
    }

    Coins(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

    /**
     * Get Coin from coins map by its value
     *
     * @param value integer value of coin
     * @return enum type Coins
     */
    public static Coins getCoin(int value) {
        return coinsMap.get(value);
    }
}
