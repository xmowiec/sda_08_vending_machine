package sda.ex;

public class Dispenser {
    private Product product;
    private int number;
    private int size;


    public Dispenser(Product product, int number) {
        this.product = product;
        this.number = number;
    }

    public Product getProduct() {
        return product;
    }

    public int getNumber() {
        return number;
    }
}
