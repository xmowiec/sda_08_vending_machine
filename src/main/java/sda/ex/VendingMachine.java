package sda.ex;

import java.util.Map;

public class VendingMachine implements IMachine {
    private String choice;
    private IContainer container;
    private int paidAmount;

    public VendingMachine() {
        container = new VendingMachineContainer();
        paidAmount = 0; // amount previously paid
    }

    @Override
    public int getPaidAmount() {
        return paidAmount;
    }

    @Override
    public void readChoice(String choice) {
        this.choice = choice;
    }

    @Override
    public IContainer getContainer() {
        return container;
    }

    @Override
    public int getAmountToPay() {
        return container.getStock().get(choice).getProduct().getPrice();
    }

    @Override
    public void insertCoin(Coins coin){
        this.paidAmount += coin.getValue();
    }

    @Override
    public boolean isEnough(){
        return this.paidAmount >= getAmountToPay();
    }

    @Override
    public Map<Coins, Integer> prepareRest() {
        IWallet wallet = new VendingMachineWallet();
        Map<Coins, Integer> rest = wallet.getCoinsMapToPayAmount(getAmountToPay()-paidAmount);
        return null;
    }
}
