package sda.ex;

import java.util.Map;

public interface IMachine {
    void readChoice(String choice);

    IContainer getContainer();

    /**
     * Get the amount should to pay for a chosen product
     */
    int getAmountToPay();

    void insertCoin(Coins coin);

    boolean isEnough();

    /**
     * Prepare map of coins with change to give
     * @return
     */
    Map<Coins,Integer> prepareRest();

    int getPaidAmount();
}
