package sda.ex;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class App {
    public static void main(String[] args) {
        IMachine vendingMachine = new VendingMachine();
        System.out.println("Wybierz produkt:");
        Map<String, Dispenser> choiceMap = vendingMachine.getContainer().getStock();
        for (String key : choiceMap.keySet()) {
            Product product = choiceMap.get(key).getProduct();
            System.out.println(String.format("%s. cena: %s, %s", key,
                    printPrettyPrice(product.getPrice()), product.getName()));
        }

        Scanner scanner = new Scanner(System.in);
        vendingMachine.readChoice(scanner.nextLine());
        int paymentAmount = vendingMachine.getAmountToPay();
        System.out.println("Kwota do zapłaty: " + printPrettyPrice(paymentAmount));

        System.out.println("==== Płatność ====");
        boolean paid = false;
        while (!paid) {
            int coinInt = scanner.nextInt();
            vendingMachine.insertCoin(getCoin(coinInt));
            System.out.println("Wpłacono " + printPrettyPrice(vendingMachine.getPaidAmount()));
            paid = vendingMachine.isEnough();
        }
        System.out.println("Wydano produkt");
        Map<Coins, Integer> rest = new HashMap<>();
        rest = vendingMachine.prepareRest();

//


        // todo show information how much money missing

        // todo show request to insert missing money

        // todo get product

        // todo chenage money
    }

    // todo count inserted coins
    private static Coins getCoin(int coinInt) {
        return Coins.getCoin(coinInt);
    }

    private static String printPrettyPrice(int number) {
        return String.format("%.2f zł", (float) number / 100);
    }
}
