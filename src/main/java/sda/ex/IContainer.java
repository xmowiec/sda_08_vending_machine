package sda.ex;

import java.util.Map;

public interface IContainer {
    int getPrice(String id);
    Map<String, Dispenser> getStock();
}
